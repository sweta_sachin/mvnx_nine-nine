package TestsOnLeftHandTabsOnCRM;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Common_Methods.CommonMethods;

public class DirectDebitsOnLeftHandMenu extends CommonMethods{
	
	
	@BeforeClass()
	public void CRM() throws InterruptedException
	{
		Login();
		SearchCustomerByAccountNumber(AccNo);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop()
	{
	  driver.quit();
		
	}
	
	@Test
	public void click_DirectDebit_tab_OnLeftHand_MenuBar() throws InterruptedException
	{
		Thread.sleep(1000);
		click_directdebit();	
		
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//*********************************** Method to Click direct debit tab ***************************************
public void click_directdebit()
{
	try {
			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
					.withTimeout(30, TimeUnit.SECONDS) 			
					.pollingEvery(5, TimeUnit.SECONDS) 			
					.ignoring(NoSuchElementException.class);
				
		WebElement notes= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/ul[1]/li[5]/a[1]"));
			
		Boolean name= notes.isDisplayed();
			
		if(name==true)  {
			    
			wait.equals(name);
			    
			Actions act= new Actions(driver);
				
			act.moveToElement(notes).doubleClick().build().perform();
		}
	
	}catch(NoSuchElementException| StaleElementReferenceException e)  {
		
		WebElement notes= driver.findElement(By.cssSelector("body.echo-page-two-level-tabs-with-sidebar.echo-layout-sidebar.ui-layout-container:nth-child(2) div.ui-layout-pane.ui-layout-pane-west.ui-layout-pane-hover.ui-layout-pane-west-hover.ui-layout-pane-open-hover.ui-layout-pane-west-open-hover:nth-child(7) div.echo-layout-region div.ui-widget.ui-widget-content.ui-corner-all.echo-region.echo-region-hideshow.echo-hideshow-open:nth-child(1) div.echo-hideshow-container div.echo-region-content div.echo-list.echo-list-vertical-unordered-with-bullets:nth-child(2) ul.ui-helper-clearfix li:nth-child(5) > a:nth-child(1)"));
			
		Actions act= new Actions(driver);
			
		act.moveToElement(notes).doubleClick().build().perform();
	}
  }
}
