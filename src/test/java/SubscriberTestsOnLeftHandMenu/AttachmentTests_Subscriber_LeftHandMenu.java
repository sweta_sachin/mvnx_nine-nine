package SubscriberTestsOnLeftHandMenu;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Common_Methods.AttachmentTests;

public class AttachmentTests_Subscriber_LeftHandMenu extends AttachmentTests {
	
	
	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		SearchByMSISDN(msisdn);
		Thread.sleep(500);
	    ClickAttachment();
	    Thread.sleep(1000);
	  	
	}
	
	@AfterClass(alwaysRun=true)
	public void end()
	{
		driver.close();
	}
	
	@Test
	public void a_Select_Document_Category_FOR_attachment_OnLefthandMenu() throws InterruptedException
	{
		Select_document_Category();
	}
	
	@Test
	public void b_Select_Document_Type_FOR_attachment_OnLefthandMenu() throws InterruptedException
	{
		Select_document_Type();
	}
	
	@Test
	public void c_Enter_name_FOR_attachment_OnLefthandMenu()
	{
		Enter_Name();
	}
	
	@Test
	public void d_Select_File_FOR_attachment_OnLefthandMenu() throws InterruptedException
	{
		
		Select_file();
	}
	
	@Test
	public void e_Click_Attach_FOR_attachment_OnLefthandMenu()
	{
		Click_attach();
	}
	
	@Test
	public void f_View_Attachments_FOR_attachment_OnLefthandMenu()
	{
		view_attachment();
	}
	
		
	@Test
	public void g_ClickEdit_attachment_FOR_attachment_OnLefthandMenu() throws InterruptedException
	{
		click_edit();
	} 
	
	@Test
	public void h_Edit_attach_new_File_FOR_attachment_OnLefthandMenu() throws InterruptedException
	{
		Attach_edit();
	}  
	
	
	@Test
	public void j_delete_attachment_FOR_attachment_OnLefthandMenu() throws InterruptedException
	{
		Delete_Attachment();
	}
	
	@Test
	public void i_Canceldelettion_FOR_attachment_OnLefthandMenu() throws InterruptedException
	{
		cancel_deletion_attachment();
	}

}
