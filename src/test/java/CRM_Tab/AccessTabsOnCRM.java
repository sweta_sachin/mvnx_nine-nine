package CRM_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Common_Methods.CommonMethods;

public class AccessTabsOnCRM extends CommonMethods{
	@BeforeClass()
	public void CRM() throws InterruptedException
	{
		Login();
		SearchCustomerByAccountNumber(AccNo);
	}
	
	@AfterClass(alwaysRun=true)
	public void stop()
	{
	  driver.quit();
		
	}
	
	@Test
	public void a_AccessEditCustomerTab() throws InterruptedException
	{
		Thread.sleep(1000);
		Access_EditCustomer();
		
	}
 
	@Test
	public void b_AccessCreateSubcriberTab() throws InterruptedException
	{
		Thread.sleep(1500);
		access_CreateSubscriber();
		
	}
	
	@Test
	public void c_AccessQuickNotesTab() throws InterruptedException
	{
		Thread.sleep(1000);
		AccessQuickNotes();
	
	}
	
	@Test
	public void d_AccessAttachmentsTab() throws InterruptedException
	{
		Thread.sleep(1000);
		AccessAttachments();
		
	}
	
	@Test
	public void e_AccessCommunicationTab() throws InterruptedException
	{
		Thread.sleep(2000);
		AccessCommunication();
		
	}
	
	@Test
	public void f_AccessNotificationTab() throws InterruptedException
	{
		Thread.sleep(1000);
		AccessNotification();
		
	}
	
	@Test
	public void g_AccessCustomerDebugDataTab() throws InterruptedException
	{
		Thread.sleep(1000);
		AccessCustomerDebugData();
		
	}
	
	@Test
	public void h_AccessDirectDebitTab() throws InterruptedException
	{
		Thread.sleep(1000);
		AccessDirectDebit();
		
	}
	
	@Test
	public void i_AccessViewInvoicesTab() throws InterruptedException
	{
		Thread.sleep(1000);
		AccessViewInvoices();
		
	}
	
	@Test
	public void j_AccessDiscountTab() throws InterruptedException
	{
		Thread.sleep(1000);
		AccessDiscount();
		
	}
	
	@Test
	public void k_AccessAd_hocCHargesTab() throws InterruptedException
	{
		Thread.sleep(1000);
		AccessAd_hocCharges();
		
	}
	
	@Test
	public void l_Accessinstant_billTab() throws InterruptedException
	{
		Thread.sleep(1000);
		Accessinstant_bill();
		
	}
	
	 @Test
	public void m_AccessDeactivateTab() throws InterruptedException
	{
		Thread.sleep(1000);
		AccessDeactivate();
		
	}  
	
	@Test
	public void n_AccessAccount_chargesTab() throws InterruptedException
	{
		Thread.sleep(1000);
		AccessAccount_charges();
		
	}
	
	@Test
	public void o_AccessFinacialAdjustmentTab() throws InterruptedException
	{
		Thread.sleep(1000);
		AccessFinacialAdjustment();
		
	}
	
	@Test
	public void p_AccessAutoBarHistoryTab() throws InterruptedException
	{
		Thread.sleep(1000);
		AccessAutoBarHistory();
		
	}
	
	@Test
	public void q_AccessNotificationExclusion_Tab() throws InterruptedException
	{
		Thread.sleep(1000);
		AccessNotificationExclusion();
		
	}
	
	@Test
	public void r_AccessSponsors_Tab() throws InterruptedException
	{
		Thread.sleep(1000);
		AccessSponsors();
		
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////		
//************************************* Method to access Edit customer *****************************
public void Access_EditCustomer() throws InterruptedException
{
	Thread.sleep(400);
										
	WebElement edit= driver.findElement(By.id("B13989778716384037"));
									
	Actions act= new Actions(driver);
										
	act.moveToElement(edit).click().build().perform();
										
	Thread.sleep(600);
										
	Boolean verify= driver.findElement(By.id("P40_BCD_FIRST_NAME")).isDisplayed();
														
	Assert.assertTrue(verify);
}
	
//************************************* Method to access Create customer *****************************
public void access_CreateSubscriber() throws InterruptedException
{
	driver.navigate().back();
				
	Thread.sleep(400);
											
	WebElement create= driver.findElement(By.id("B13989961185388418"));
										
	Actions act= new Actions(driver);
											
	act.moveToElement(create).click().build().perform();
											
	Thread.sleep(600);
											
	Boolean verify= driver.findElement(By.id("P34_BSD_NAME")).isDisplayed();
															
	Assert.assertTrue(verify);

}
	
//**********************Method to test Access Quick notes ****************************************************	
public void AccessQuickNotes() throws InterruptedException
{
	driver.navigate().back();
				
	Thread.sleep(400);
			
	WebElement QuickNotes= driver.findElement(By.id("B13528763358177637"));
				
	Actions act= new Actions(driver);
				
	act.moveToElement(QuickNotes).click().build().perform();
				
	Thread.sleep(600);
			
	Boolean verify= driver.findElement(By.id("B13535157679207053")).isDisplayed();
														
	Assert.assertTrue(verify);
	
}
	
//**********************Method to test Access attachment****************************************************	
public void AccessAttachments() throws InterruptedException
{
	driver.navigate().back();
			
	Thread.sleep(400);
			
	WebElement attachment= driver.findElement(By.id("B8981814074129539"));
		   
    Actions act= new Actions(driver);
			
    act.moveToElement(attachment).doubleClick().build().perform();
			
    Thread.sleep(500);
			
    Boolean verify= driver.findElement(By.id("P59_DOC_DOCT_UID")).isDisplayed();
			
	Assert.assertTrue(verify);

}
	
//**********************Method to test Access communication****************************************************	
public void AccessCommunication() throws InterruptedException
{
	driver.navigate().back();
			
	Thread.sleep(400);
	
	WebElement comms= driver.findElement(By.id("B43693003878117959"));
			
	Actions act= new Actions(driver);
			
	act.moveToElement(comms).click().build().perform();
			
	Thread.sleep(500);
				
	Boolean verify= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[1]/div[1]")).isDisplayed();
				
	Assert.assertTrue(verify);

}

//**********************Method to test Access notification****************************************************	
 public void AccessNotification() throws InterruptedException
{
 	driver.navigate().back();
	 	
 	Thread.sleep(400);
	 	
 	WebElement notes= driver.findElement(By.id("B12119121652547560"));
		  
	Actions act= new Actions(driver);
		
	act.moveToElement(notes).doubleClick().build().perform();
		
	Thread.sleep(500);
		
	Boolean verify= driver.findElement(By.id("B12131301940768901")).isDisplayed();
		
	Assert.assertTrue(verify);
		    
	}
	
//**********************Method to test Access Customer DebugData****************************************************	
public void AccessCustomerDebugData() throws InterruptedException
{
	driver.navigate().back();
		
	Thread.sleep(400);
		
	WebElement notes= driver.findElement(By.id("B32340706460922145"));
		 
	Actions act= new Actions(driver);
		
	act.moveToElement(notes).doubleClick().build().perform();
		
	Thread.sleep(500);
			
	Boolean verify= driver.findElement(By.xpath("//div[@id='R14491121614688658']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']")).isDisplayed();
		
	Assert.assertTrue(verify);
		
}
	
//**********************Method to test Access Direct debit****************************************************		
public void AccessDirectDebit() throws InterruptedException
{  
	driver.navigate().back();
		
	Thread.sleep(400);
	
	WebElement notes= driver.findElement(By.id("B62690930510264602"));
		
	Actions act= new Actions(driver);
		
	act.moveToElement(notes).doubleClick().build().perform();
						
	Thread.sleep(500);
		
	Boolean verify= driver.findElement(By.xpath("//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']")).isDisplayed();
		
	Assert.assertTrue(verify);
		
}
	
//**********************Method to test view invoices****************************************************		
public void AccessViewInvoices() throws InterruptedException
{
	driver.navigate().back();
		
	Thread.sleep(400);
	
	WebElement notes= driver.findElement(By.id("B31575309173114291"));
		    
	Actions act= new Actions(driver);
		
	act.moveToElement(notes).doubleClick().build().perform();
		
	Thread.sleep(500);
		
	Boolean verify= driver.findElement(By.id("B16366916284812711")).isDisplayed();
		
	Assert.assertTrue(verify);
		    
}

//**********************Method to test Access Discounts****************************************************	
public void AccessDiscount() throws InterruptedException
{
	driver.navigate().back();
		
	Thread.sleep(400);
	
	WebElement notes= driver.findElement(By.id("B5018025327972823"));
	
	Actions act= new Actions(driver);
		
	act.moveToElement(notes).doubleClick().build().perform();
		
	Thread.sleep(500);
		
	Boolean verify= driver.findElement(By.id("B5025316912188034")).isDisplayed();
		
	Assert.assertTrue(verify);
		  
	}
	
//**********************Method to test Access ad-hoc charges****************************************************		
public void AccessAd_hocCharges() throws InterruptedException
{
 	driver.navigate().back();
	 	
 	Thread.sleep(400);
	
	WebElement notes= driver.findElement(By.id("B50770423001886018"));
		
	Actions act= new Actions(driver);
	
	act.moveToElement(notes).doubleClick().build().perform();
		
	Thread.sleep(500);
		
	Boolean verify= driver.findElement(By.id("B83684304723539385")).isDisplayed();
			
	Assert.assertTrue(verify);
		    
}
	
//**********************Method to test Access Customer instant bill****************************************************	
public void Accessinstant_bill() throws InterruptedException
{
	driver.navigate().back();
		 
	Thread.sleep(400);
		 
	WebElement notes= driver.findElement(By.id("B43652226619858837"));
		   
	Actions act= new Actions(driver);
			
	act.moveToElement(notes).doubleClick().build().perform();
			
	Thread.sleep(500);
		 
	Boolean verify= driver.findElement(By.id("P18_BILL_RUN_TYPE")).isDisplayed();
			
	Assert.assertTrue(verify);
			
 }
	
//**********************Method to test Access deactivate tab ****************************************************		
public void AccessDeactivate() throws InterruptedException
{
	 driver.navigate().back();
		 
	 Thread.sleep(400);
		 
	 WebElement notes= driver.findElement(By.linkText("Deactivate"));
		   
	 Actions act= new Actions(driver);
			
	 act.moveToElement(notes).doubleClick().build().perform();
			
	 Thread.sleep(500);
			
	 Boolean verify= driver.findElement(By.id("P82_ACCOUNT_2")).isDisplayed();
			
	 Assert.assertTrue(verify);  
	   
}
	
//**********************Method to test Access account charges****************************************************	
public void AccessAccount_charges() throws InterruptedException
{
	driver.navigate().back();
		
	Thread.sleep(400);
		
	WebElement notes= driver.findElement(By.id("B75658413721424833"));
		  
	Actions act= new Actions(driver);
		
	act.moveToElement(notes).doubleClick().build().perform();
		
	Thread.sleep(500);
		
	Boolean verify= driver.findElement(By.id("B75601926355290154")).isDisplayed();
			
	Assert.assertTrue(verify);  	
		
}
	
//**********************Method to test Access Financial adjustment ****************************************************	
public void AccessFinacialAdjustment() throws InterruptedException
{
	driver.navigate().back();
		
	Thread.sleep(400);
		
	WebElement notes= driver.findElement(By.id("B75722923293888088"));
		   
	Actions act= new Actions(driver);
			
	act.moveToElement(notes).doubleClick().build().perform();
			
	Thread.sleep(500);
			
	Boolean verify= driver.findElement(By.id("B75718916798558153")).isDisplayed();
			
	Assert.assertTrue(verify);  
		    
}
	
//**********************Method to test Access Autobar history ****************************************************	
public void AccessAutoBarHistory() throws InterruptedException
{
	driver.navigate().back();
		
	Thread.sleep(400);
		
	WebElement notes= driver.findElement(By.id("B76885014023393099"));
		  
	Actions act= new Actions(driver);
		
	act.moveToElement(notes).doubleClick().build().perform();
		
	Thread.sleep(500);
		
	Boolean verify= driver.findElement(By.id("P120_START_DATE")).isDisplayed();
		
	Assert.assertTrue(verify);  
		    
}

//**********************Method to test Access Notification exclusion****************************************************	
public void AccessNotificationExclusion() throws InterruptedException
{
	driver.navigate().back();
		
	Thread.sleep(400);
		
	WebElement notes= driver.findElement(By.id("B78499006552606022"));
		   
	Actions act= new Actions(driver);
		
	act.moveToElement(notes).doubleClick().build().perform();
		
	Thread.sleep(500);
			
	Boolean verify= driver.findElement(By.id("P112_TTNE_TT_UID")).isDisplayed();
		
	Assert.assertTrue(verify);      

}
	
//**********************Method to test Access sponsors  ****************************************************	
public void AccessSponsors() throws InterruptedException
{
	driver.navigate().back();
		
	Thread.sleep(400);
		
	WebElement notes= driver.findElement(By.id("B91951123054272107"));
		   
	Actions act= new Actions(driver);
		
	act.moveToElement(notes).doubleClick().build().perform();
		
	Thread.sleep(500);
			
	Boolean verify= driver.findElement(By.id("B16541864605415761")).isDisplayed();
		
	Assert.assertTrue(verify);      
	
}
	
//**********************Method to test Access Customer history ****************************************************	
public void AccessCustomerHistory() throws InterruptedException
{
	WebElement notes= driver.findElement(By.xpath("//span[@class='ui-icon ui-icon-circle-triangle-s']"));
		  
    Actions act= new Actions(driver);
			
    act.moveToElement(notes).click().build().perform();
			
    Thread.sleep(500);
			
    Boolean verify= driver.findElement(By.xpath("//div[@id='R43994218907620125']//tr[3]//td[2]//span[1]")).isDisplayed();
			
	Assert.assertTrue(verify);   

}

}
